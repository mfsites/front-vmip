const menuPrincipal = $('#menuPrincipal');
const inptAssunto = $('#inptAssunto');
const inptNome = $('#inptNome');
const inptEmail = $('#inptEmail');
const btnEnviar = $('#btnEnviar');

$(document).ready(function() {

	$(window).on('scroll', function () {

		alturaImg = $('.imgLogoInicial').height();
		divInvisivel = $('#idDivInvisivel');

		if ($(window).scrollTop() < alturaImg) {
			menuPrincipal.removeClass('menuFixo');
			divInvisivel.removeClass('margemFixar');
			return;
		}
	
		menuPrincipal.addClass('menuFixo');
		divInvisivel.addClass('margemFixar');
	
	});

	$('#btnProduto1').on('click', function() {
		inptAssunto.val('Orçamento Relógio Comparador Digital ABSOLUTE');
		inptAssunto.focus();
	});

	$('#btnProduto2').on('click', function() {
		inptAssunto.val('Orçamento Cabeçote Digital Micrométrico');
		inptAssunto.focus();
	});

	$('#btnProduto3').on('click', function() {
		inptAssunto.val('Orçamento Paquímetro Digital ABSOLUTE');
		inptAssunto.focus();
	});

	$('#btnProduto4').on('click', function() {
		inptAssunto.val('Orçamento Traçador de Altura Digital');
		inptAssunto.focus();
	});

	$('#btnEnviar').on('click', function() {

		var txtCampoObrigatorio = '';
		var teste = '';

		if (inptNome.val() == '' || inptEmail.val() == '') {
			if (inptNome.val() != '')
				txtCampoObrigatorio = 'Campo e-mail é obrigatório';
			else if (inptEmail.val() != '')
				txtCampoObrigatorio = 'Campo nome é obrigatório';
			else
				txtCampoObrigatorio = 'Campos nome e e-mail são obrigatórios';

			alert(txtCampoObrigatorio)
		}

	});

})