export class Usuario {

    public nome: String;
    public sobrenome: String;
    public email: String;
    public senha: String;

    constructor() {
        this.nome = "";
        this.sobrenome = "";
        this.email = "";
        this.senha = "";
    }

}