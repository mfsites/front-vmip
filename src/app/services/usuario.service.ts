import { Injectable } from '@angular/core';
import { Usuario } from '../classes/usuario';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

    constructor() { }

    public getUsuario(): Usuario {
        let usuario = new Usuario();
        usuario.nome = 'Administrador';
        usuario.sobrenome = 'Admin';
        usuario.email = 'admin';
        usuario.senha = '123';

        return usuario;
    }

    public listaUsuario(): Usuario[] {
        return [
            { nome: "Leandro", sobrenome: "Finochio", email: "lefinochio@hotmail.com", senha: "" },
            { nome: "Carolina", sobrenome: "Finochio", email: "carol@hotmail.com", senha: "" },
            { nome: "Arthur", sobrenome: "", email: "arthur@hotmail.com", senha: "" },
        ]
    }
}
