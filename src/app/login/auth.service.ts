import { Injectable, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { UsuarioService } from '../services/usuario.service';
import { Usuario } from '../classes/usuario';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

    private usuarioAutenticado: boolean = false;
    mostrarMenuEmitter = new EventEmitter<boolean>();

    constructor(private usuarioService:UsuarioService, private router: Router) { }

    autenticar(usuario: Usuario) {
        if (usuario.email === this.usuarioService.getUsuario().email && usuario.senha === this.usuarioService.getUsuario().senha) {
            this.usuarioAutenticado = true;
            this.mostrarMenuEmitter.emit(true);
            this.router.navigate(['/'])
        } else {
            usuario.email === this.usuarioService.getUsuario().email ? alert('Senha incorreta') : alert('Nenhum usuário cadastrado com esse e-mail');
            this.usuarioAutenticado = false;
            this.mostrarMenuEmitter.emit(false);
        }
    }

}