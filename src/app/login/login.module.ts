import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login.component';
import { LoginRoutingModule } from './login.routing.module';
import { UsuarioCadastroComponent } from '../usuario/cadastro/usuario-cadastro.component';
import { FormsModule } from '@angular/forms';
import { CheckboxModule } from 'primeng/checkbox';

@NgModule({
  declarations: [LoginComponent, UsuarioCadastroComponent],
  imports: [
    CommonModule,
    LoginRoutingModule,
    FormsModule,
    CheckboxModule
  ]
})
export class LoginModule { }
