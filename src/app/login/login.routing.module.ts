import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login.component';
import { UsuarioCadastroComponent } from '../usuario/cadastro/usuario-cadastro.component';

const routes: Routes = [
    { 
        path: '', 
        component: LoginComponent 
    },
    {
        path: 'cadastrar',
        component: UsuarioCadastroComponent
    }
];

@NgModule({
    declarations: [],
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LoginRoutingModule { }
