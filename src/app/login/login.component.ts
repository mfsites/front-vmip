import { Component, OnInit } from '@angular/core';
import { AuthService } from './auth.service';
import { Usuario } from '../classes/usuario';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

    public usuarios: Usuario[];
    private usuario: Usuario = new Usuario();

    constructor(private authService: AuthService ) { }

    ngOnInit() {
    }

    logar() {
        this.authService.autenticar(this.usuario)
    }

}