import { Component, OnInit } from '@angular/core';
import { UsuarioService } from '../services/usuario.service';
import { Usuario } from '../classes/usuario';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.css']
})
export class PerfilComponent implements OnInit {

    private usuario: Usuario = this.usuarioService.getUsuario();

    constructor(private usuarioService: UsuarioService) { }

    ngOnInit() {
    }

}
