import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
    { 
        path: 'home', 
        loadChildren: './home/home.module#HomeModule' 
    },
    {
        path: 'contato',
        loadChildren: './contato/contato.module#ContatoModule'
    },
    {
        path: 'login',
        loadChildren: './login/login.module#LoginModule'
    },
    {
        path: 'loja',
        loadChildren: './loja/loja.module#LojaModule'
    },
    {
        path: 'perfil',
        loadChildren: './perfil/perfil.module#PerfilModule'
    },
    { 
        path: 'servico', 
        loadChildren: './servico/servico.module#ServicoModule' 
    },
    { 
        path: 'sobre', 
        loadChildren: './sobre/sobre.module#SobreModule' 
    },
    { 
        path: '',
        pathMatch: 'full',
        redirectTo: '/home'
    }
];

@NgModule({
    declarations: [],
    imports: [RouterModule.forRoot(routes, { useHash: true })],
    exports: [RouterModule]
})
export class AppRoutingModule { }
