import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LojaComponent } from './loja.component';
import { LojaRoutingModule } from './loja.routing.module';

@NgModule({
  declarations: [LojaComponent],
  imports: [
    CommonModule,
    LojaRoutingModule
  ]
})
export class LojaModule { }
