import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LojaComponent } from './loja.component';

const routes: Routes = [
    { 
        path: '', 
        component: LojaComponent
    }
];

@NgModule({
    declarations: [],
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LojaRoutingModule { }
