import { Component, OnInit } from '@angular/core';
import { Usuario } from '../classes/usuario';
import { UsuarioService } from '../services/usuario.service';

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
  styleUrls: ['./usuario.component.css']
})
export class UsuarioComponent implements OnInit {

    public usuarios: Usuario[];
    public usuario: Usuario = new Usuario();
    constructor(private usuarioService:UsuarioService) { }

    ngOnInit() {
        this.usuarios = this.usuarioService.listaUsuario();
    }

    public logar() {
        alert(`Usuário ${this.usuario.email} está logado.`)
    }

}
