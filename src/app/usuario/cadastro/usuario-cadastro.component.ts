import { Component, OnInit } from '@angular/core';
import { Usuario } from 'src/app/classes/usuario';

@Component({
  selector: 'app-usuario-cadastro',
  templateUrl: './usuario-cadastro.component.html',
  styleUrls: ['./usuario-cadastro.component.css']
})
export class UsuarioCadastroComponent implements OnInit {

    public usuario: Usuario = new Usuario();
    value: boolean = false;
    
    constructor() { }

    ngOnInit() {
    }

    salvar() {
        alert(`Usuário ${this.usuario.nome} salvo com sucesso `)
    };

}