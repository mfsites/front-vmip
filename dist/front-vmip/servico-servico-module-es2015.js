(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["servico-servico-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/servico/servico.component.html":
/*!**************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/servico/servico.component.html ***!
  \**************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"content\">\r\n    <h5>Conheça os nossos serviços</h5>\r\n    <p>\r\n        Calibração e manutenção preventiva e corretiva de instrumentos de precisão. Algumas das grandezas com que trabalhamos:\r\n    </p>\r\n    <ul class=\"row\">\r\n        <li class=\"col-sm-2\">Dimensional</li>\r\n        <li class=\"col-sm-2\">Eletricos</li>\r\n        <li class=\"col-sm-2\">Força</li>\r\n        <li class=\"col-sm-2\">Massa</li>\r\n        <li class=\"col-sm-3\">Pressão</li>\r\n        <li class=\"col-sm-2\">Temperatura</li>\r\n        <li class=\"col-sm-2\">Torque</li>\r\n        <li class=\"col-sm-2\">Umidade</li>\r\n        <li class=\"col-sm-2\">Vazão</li>\r\n        <li class=\"col-sm-2\">Viscosidade</li>\r\n    </ul>\r\n    <p>\r\n        Alguns equipamentos em realizamos manutenção:\r\n    </p>\r\n    <ul class=\"row\">\r\n        <li class=\"col-sm-2\">Balança</li>\r\n        <li class=\"col-sm-3\">Durômetro Brinell</li>\r\n        <li class=\"col-sm-2\">Durômetro Rockwell</li>\r\n        <li class=\"col-sm-2\">Durômetro Vickers</li>\r\n        <li class=\"col-sm-2\">Imicron</li>\r\n        <li class=\"col-sm-2\">Manômetro</li>\r\n        <li class=\"col-sm-3\">Máquina de tração/compressão</li>\r\n        <li class=\"col-sm-2\">Medidor de Camadas</li>\r\n        <li class=\"col-sm-2\">Micrômetro</li>\r\n        <li class=\"col-sm-2\">Multímetro</li>\r\n        <li class=\"col-sm-2\">Paquímetro</li>\r\n        <li class=\"col-sm-3\">Projetor e Perfil</li>\r\n        <li class=\"col-sm-2\">Relógio Apalpador</li>\r\n        <li class=\"col-sm-2\">Relógio Comparador</li>\r\n        <li class=\"col-sm-2\">Rugosímetro</li>\r\n        <li class=\"col-sm-2\">Súbito</li>\r\n        <li class=\"col-sm-2\">Viscosímetro</li>\r\n    </ul>\r\n    <h5>Politica de Qualidade</h5>\r\n    <p>\r\n        Oferece ao mercado somente produtos e serviços que garantam a satisfação do cliente.<br>\r\n        Operar processos confiáveis e capazes de atender as especificações das normas vigentes.<br>\r\n        Ter como objetivo a qualidade total dos nossos serviços.\r\n    </p>\r\n</div>");

/***/ }),

/***/ "./src/app/servico/servico.component.css":
/*!***********************************************!*\
  !*** ./src/app/servico/servico.component.css ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".content {\r\n    color: #05053c;\r\n    padding: 0 50px 30px;\r\n}\r\n\r\n.content h5 {\r\n    font-weight: bold;\r\n    margin-bottom: 1.5rem;\r\n}\r\n\r\n.content ul {\r\n    padding-left: 15px;\r\n    margin-bottom: 2rem;\r\n}\r\n\r\n.content ul li {\r\n    font-weight: bold;\r\n    min-width: -webkit-fit-content;\r\n    min-width: -moz-fit-content;\r\n    min-width: fit-content;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2Vydmljby9zZXJ2aWNvLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxjQUFjO0lBQ2Qsb0JBQW9CO0FBQ3hCOztBQUVBO0lBQ0ksaUJBQWlCO0lBQ2pCLHFCQUFxQjtBQUN6Qjs7QUFFQTtJQUNJLGtCQUFrQjtJQUNsQixtQkFBbUI7QUFDdkI7O0FBRUE7SUFDSSxpQkFBaUI7SUFDakIsOEJBQXNCO0lBQXRCLDJCQUFzQjtJQUF0QixzQkFBc0I7QUFDMUIiLCJmaWxlIjoic3JjL2FwcC9zZXJ2aWNvL3NlcnZpY28uY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jb250ZW50IHtcclxuICAgIGNvbG9yOiAjMDUwNTNjO1xyXG4gICAgcGFkZGluZzogMCA1MHB4IDMwcHg7XHJcbn1cclxuXHJcbi5jb250ZW50IGg1IHtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMS41cmVtO1xyXG59XHJcblxyXG4uY29udGVudCB1bCB7XHJcbiAgICBwYWRkaW5nLWxlZnQ6IDE1cHg7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAycmVtO1xyXG59XHJcblxyXG4uY29udGVudCB1bCBsaSB7XHJcbiAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgIG1pbi13aWR0aDogZml0LWNvbnRlbnQ7XHJcbn0iXX0= */");

/***/ }),

/***/ "./src/app/servico/servico.component.ts":
/*!**********************************************!*\
  !*** ./src/app/servico/servico.component.ts ***!
  \**********************************************/
/*! exports provided: ServicoComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ServicoComponent", function() { return ServicoComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let ServicoComponent = class ServicoComponent {
    constructor() { }
    ngOnInit() {
    }
};
ServicoComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-servico',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./servico.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/servico/servico.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./servico.component.css */ "./src/app/servico/servico.component.css")).default]
    })
], ServicoComponent);



/***/ }),

/***/ "./src/app/servico/servico.module.ts":
/*!*******************************************!*\
  !*** ./src/app/servico/servico.module.ts ***!
  \*******************************************/
/*! exports provided: ServicoModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ServicoModule", function() { return ServicoModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _servico_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./servico.component */ "./src/app/servico/servico.component.ts");
/* harmony import */ var _servico_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./servico.routing.module */ "./src/app/servico/servico.routing.module.ts");





let ServicoModule = class ServicoModule {
};
ServicoModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [_servico_component__WEBPACK_IMPORTED_MODULE_3__["ServicoComponent"]],
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _servico_routing_module__WEBPACK_IMPORTED_MODULE_4__["ServicoRoutingModule"]
        ]
    })
], ServicoModule);



/***/ }),

/***/ "./src/app/servico/servico.routing.module.ts":
/*!***************************************************!*\
  !*** ./src/app/servico/servico.routing.module.ts ***!
  \***************************************************/
/*! exports provided: ServicoRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ServicoRoutingModule", function() { return ServicoRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _servico_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./servico.component */ "./src/app/servico/servico.component.ts");




const routes = [
    {
        path: '',
        component: _servico_component__WEBPACK_IMPORTED_MODULE_3__["ServicoComponent"]
    }
];
let ServicoRoutingModule = class ServicoRoutingModule {
};
ServicoRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [],
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], ServicoRoutingModule);



/***/ })

}]);
//# sourceMappingURL=servico-servico-module-es2015.js.map