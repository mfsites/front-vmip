(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["home-home-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/home/home.component.html":
/*!********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/home/home.component.html ***!
  \********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<menu></menu>\r\n<div class=\"row\">\r\n    <div class=\"col-sm-12\">\r\n        <div class=\"row linhaComInfos\">\r\n            <div class=\"col-sm-4 divImgsComDescricao\">\r\n                <div>\r\n                    <div>\r\n                        <img src=\"../../assets/inicio/topoSobreNos.png\" title=\"Sobre nós\" alt=\"Sobre nós\" max-height=\"45\" class=\"imgsSobre\" />\r\n                    </div>\r\n                    <div href=\"\" class=\"marginImgA\">\r\n                        <img src=\"../../assets/inicio/sobreNos.png\" title=\"Sobre nós\" alt=\"Sobre nós\" width=\"100%\" style=\"margin: 15px 0;\"/>\r\n                    </div>\r\n                    <span class=\"spanMedio\">\r\n                        Quer conhecer um pouco mais sobre a VMIP? Confira uma história que vem sendo construída a mais de 10 anos \r\n                        e que não para de inovar na qualidade e atendimento rápido.\r\n                    </span>\r\n                </div>\r\n                <a class=\"spanSaibaMais\">\r\n                    SAIBA MAIS\r\n                </a>\r\n            </div>\r\n            <div class=\"col-sm-4 divImgsComDescricao\">\r\n                <div>\r\n                    <div>\r\n                        <img src=\"../../assets/inicio/topoContato.png\" title=\"Contato\" alt=\"Contato\" max-height=\"45\" class=\"imgsSobre\" />\r\n                    </div>\r\n                    <div class=\"marginImgA\">\r\n                        <img src=\"../../assets/inicio/contato.png\" title=\"Contato\" alt=\"Contato\" width=\"100%\" style=\"margin: 15px 0;\"/>\r\n                    </div>\r\n                    <span class=\"spanMedio\">\r\n                        Entre em contato conosco e realize um orçamento rápido com valores especiais para sua empresa.\r\n                    </span>\r\n                </div>\r\n                <a class=\"spanSaibaMais\">\r\n                    SAIBA MAIS\r\n                </a>\r\n            </div>\r\n            <div class=\"col-sm-4 divImgsComDescricao\">\r\n                <div>\r\n                    <div>\r\n                        <img src=\"../../assets/inicio/topoNossasOfertas.png\" title=\"Nossas ofertas\" alt=\"Nossa loja\" max-height=\"45\" class=\"imgsSobre\" />\r\n                    </div>\r\n                    <div href=\"\" class=\"marginImgA\">\r\n                        <img src=\"../../assets/inicio/nossasOfertas.png\" title=\"Nossas ofertas\" alt=\"Nossa loja\" width=\"100%\" style=\"margin: 15px 0;\"/>\r\n                    </div>\r\n                    <span class=\"spanMedio\">\r\n                        Se é qualidade e preço baixo que está procurando, aqui é o lugar certo, acesse nossa loja online e compre já tudo que \r\n                        precisa em instrumentos de medição.\r\n                    </span>\r\n                </div>\r\n                <a class=\"spanSaibaMais\">\r\n                    SAIBA MAIS\r\n                </a>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n<div class=\"row p30e0\">\r\n    <div class=\"col-sm-12\">\r\n        <div class=\"divSpanProdutosDestaque\">PRODUTOS EM DESTAQUE</div>\r\n    </div>\r\n    <div class=\"row corFundoLinhaProdutosDestaque m0e15\">\r\n        <div class=\"col-sm-3 divColsProdutosDestaque\">\r\n            <div class=\"divProdutoEmDestaque\">\r\n                <div class=\"bordaCinza\">\r\n                    <img src=\"../../assets/produtos/produto1.jpg\" alt=\"\" width=\"100%\" />\r\n                </div>\r\n                <span class=\"spanPequeno m20e0\">\r\n                    Relógio Comparador Digital ABSOLUTE 12,7mm 0,01mm ID-CX Tampa Lisa Com Preset 543-400B\r\n                </span>\r\n                <button class=\"btnComprar\" ng-click=\"fazerOrcamento('Relógio Comparador Digital ABSOLUTE')\">Faça um orçamento</button>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-sm-3 divColsProdutosDestaque\">\r\n            <div class=\"divProdutoEmDestaque\">\r\n                <div class=\"bordaCinza\">\r\n                    <img src=\"../../assets/produtos/produto2.jpg\" alt=\"\" width=\"100%\" />\r\n                </div>\r\n                <span class=\"spanPequeno m20e0\">\r\n                    Cabeçote Digital Micrométrico 50mm/0,001mm – 164-163\r\n                </span>\r\n                <button class=\"btnComprar\" ng-click=\"fazerOrcamento('Cabeçote Digital Micrométrico')\">Faça um orçamento</button>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-sm-3 divColsProdutosDestaque\">\r\n            <div class=\"divProdutoEmDestaque\">\r\n                <div class=\"bordaCinza\">\r\n                    <img src=\"../../assets/produtos/produto3.jpg\" alt=\"\" width=\"100%\" />\r\n                </div>\r\n                <span class=\"spanPequeno m20e0\">\r\n                    Paquímetro Digital ABSOLUTE 150mm 0,01mm Sem Saída de Dados 500-196-30B\r\n                </span>\r\n                <button class=\"btnComprar\" ng-click=\"fazerOrcamento('Paquímetro Digital ABSOLUTE')\">Faça um orçamento</button>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-sm-3 divColsProdutosDestaque\">\r\n            <div class=\"divProdutoEmDestaque\">\r\n                <div class=\"bordaCinza\">\r\n                    <img src=\"../../assets/produtos/produto4.jpg\" alt=\"\" width=\"100%\" />\r\n                </div>\r\n                <span class=\"spanPequeno m20e0\">\r\n                    Traçador de Altura Digital 300mm 0,005mm Modelo Padrão Com Saída de Dados Para CEP 192-613-10\r\n                </span>\r\n                <button class=\"btnComprar\" ng-click=\"fazerOrcamento('Traçador de Altura Digital')\">Faça um orçamento</button>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>");

/***/ }),

/***/ "./src/app/home/home.component.css":
/*!*****************************************!*\
  !*** ./src/app/home/home.component.css ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".linhaComInfos {\r\n    background-color: #e9ecef63;\r\n}\r\n\r\n.divImgsComDescricao {\r\n    display: flex;\r\n    flex-direction: column;\r\n    align-items: center;\r\n    text-align: center;\r\n    padding: 0 30px 30px;\r\n    justify-content: space-between;\r\n}\r\n\r\n.imgsSobre {\r\n    min-width: 152px;\r\n    width: 100%;\r\n    max-width: 400px;\r\n}\r\n\r\n.spanMedio {\r\n    font-size: 1.125rem; /*18px*/\r\n    color: #616160;\r\n}\r\n\r\n.spanSaibaMais {\r\n    background-color: #002753;\r\n    color: #fff !important;\r\n    padding: 14px 20px;\r\n    border-radius: 20px;\r\n    cursor: pointer;\r\n    font-family: 'Roboto', sans-serif;\r\n    letter-spacing: .07em;\r\n    font-weight: 700;\r\n    font-size: 12px;\r\n    -webkit-border-radius: 20px;\r\n    box-shadow: none;\r\n    margin-top: 15px;\r\n}\r\n\r\n.spanSaibaMais:hover {\r\n        background-color: #05053c;\r\n    }\r\n\r\n.p30e0 {\r\n    padding: 30px 0;\r\n}\r\n\r\n.m0e15 {\r\n    margin: 0 15px;\r\n}\r\n\r\n.m20e0 {\r\n    margin: 20px 0;\r\n}\r\n\r\n.divSpanProdutosDestaque {\r\n    color: #616160;\r\n    font-weight: 700;\r\n    letter-spacing: 5px;\r\n    font-size: 1.25rem;\r\n    border-bottom: 1px solid #616160;\r\n    margin-bottom: 15px;\r\n    padding-bottom: 10px;\r\n}\r\n\r\n.divColsProdutosDestaque {\r\n    display: grid;\r\n    text-align: center;\r\n    padding: 15px;\r\n    justify-content: space-between;\r\n    min-width: 240px;\r\n}\r\n\r\n.btnComprar {\r\n    cursor: pointer;\r\n    background-color: #1BB258;\r\n    color: #fff;\r\n    border-radius: 6px;\r\n    border: none;\r\n    padding: 10px 0;\r\n    margin-top: 30px;\r\n    max-height: 41px;\r\n}\r\n\r\n.btnComprar:hover {\r\n        background-color: #139447;\r\n    }\r\n\r\n.divProdutoEmDestaque {\r\n    display: flex;\r\n    background-color: #fff;\r\n    padding: 10px 25px;\r\n    border-radius: 3px;\r\n    flex-direction: column;\r\n    justify-content: space-between;\r\n}\r\n\r\n.corFundoLinhaProdutosDestaque {\r\n    background-color: #ececec;\r\n}\r\n\r\n.bordaCinza {\r\n    border: 1px solid #a7a7a4;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaG9tZS9ob21lLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSwyQkFBMkI7QUFDL0I7O0FBRUE7SUFDSSxhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLG1CQUFtQjtJQUNuQixrQkFBa0I7SUFDbEIsb0JBQW9CO0lBQ3BCLDhCQUE4QjtBQUNsQzs7QUFFQTtJQUNJLGdCQUFnQjtJQUNoQixXQUFXO0lBQ1gsZ0JBQWdCO0FBQ3BCOztBQUVBO0lBQ0ksbUJBQW1CLEVBQUUsT0FBTztJQUM1QixjQUFjO0FBQ2xCOztBQUVBO0lBQ0kseUJBQXlCO0lBQ3pCLHNCQUFzQjtJQUN0QixrQkFBa0I7SUFDbEIsbUJBQW1CO0lBQ25CLGVBQWU7SUFDZixpQ0FBaUM7SUFDakMscUJBQXFCO0lBQ3JCLGdCQUFnQjtJQUNoQixlQUFlO0lBQ2YsMkJBQTJCO0lBRTNCLGdCQUFnQjtJQUNoQixnQkFBZ0I7QUFDcEI7O0FBRUk7UUFDSSx5QkFBeUI7SUFDN0I7O0FBRUo7SUFDSSxlQUFlO0FBQ25COztBQUVBO0lBQ0ksY0FBYztBQUNsQjs7QUFFQTtJQUNJLGNBQWM7QUFDbEI7O0FBRUE7SUFDSSxjQUFjO0lBQ2QsZ0JBQWdCO0lBQ2hCLG1CQUFtQjtJQUNuQixrQkFBa0I7SUFDbEIsZ0NBQWdDO0lBQ2hDLG1CQUFtQjtJQUNuQixvQkFBb0I7QUFDeEI7O0FBRUE7SUFDSSxhQUFhO0lBQ2Isa0JBQWtCO0lBQ2xCLGFBQWE7SUFDYiw4QkFBOEI7SUFDOUIsZ0JBQWdCO0FBQ3BCOztBQUVBO0lBQ0ksZUFBZTtJQUNmLHlCQUF5QjtJQUN6QixXQUFXO0lBQ1gsa0JBQWtCO0lBQ2xCLFlBQVk7SUFDWixlQUFlO0lBQ2YsZ0JBQWdCO0lBQ2hCLGdCQUFnQjtBQUNwQjs7QUFFSTtRQUNJLHlCQUF5QjtJQUM3Qjs7QUFFSjtJQUNJLGFBQWE7SUFDYixzQkFBc0I7SUFDdEIsa0JBQWtCO0lBQ2xCLGtCQUFrQjtJQUNsQixzQkFBc0I7SUFDdEIsOEJBQThCO0FBQ2xDOztBQUVBO0lBQ0kseUJBQXlCO0FBQzdCOztBQUVBO0lBQ0kseUJBQXlCO0FBQzdCIiwiZmlsZSI6InNyYy9hcHAvaG9tZS9ob21lLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubGluaGFDb21JbmZvcyB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZTllY2VmNjM7XHJcbn1cclxuXHJcbi5kaXZJbWdzQ29tRGVzY3JpY2FvIHtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIHBhZGRpbmc6IDAgMzBweCAzMHB4O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG59XHJcblxyXG4uaW1nc1NvYnJlIHtcclxuICAgIG1pbi13aWR0aDogMTUycHg7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIG1heC13aWR0aDogNDAwcHg7XHJcbn1cclxuXHJcbi5zcGFuTWVkaW8ge1xyXG4gICAgZm9udC1zaXplOiAxLjEyNXJlbTsgLyoxOHB4Ki9cclxuICAgIGNvbG9yOiAjNjE2MTYwO1xyXG59XHJcblxyXG4uc3BhblNhaWJhTWFpcyB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDAyNzUzO1xyXG4gICAgY29sb3I6ICNmZmYgIWltcG9ydGFudDtcclxuICAgIHBhZGRpbmc6IDE0cHggMjBweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDIwcHg7XHJcbiAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICBmb250LWZhbWlseTogJ1JvYm90bycsIHNhbnMtc2VyaWY7XHJcbiAgICBsZXR0ZXItc3BhY2luZzogLjA3ZW07XHJcbiAgICBmb250LXdlaWdodDogNzAwO1xyXG4gICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgLXdlYmtpdC1ib3JkZXItcmFkaXVzOiAyMHB4O1xyXG4gICAgLXdlYmtpdC1ib3gtc2hhZG93OiBub25lO1xyXG4gICAgYm94LXNoYWRvdzogbm9uZTtcclxuICAgIG1hcmdpbi10b3A6IDE1cHg7XHJcbn1cclxuXHJcbiAgICAuc3BhblNhaWJhTWFpczpob3ZlciB7XHJcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzA1MDUzYztcclxuICAgIH1cclxuXHJcbi5wMzBlMCB7XHJcbiAgICBwYWRkaW5nOiAzMHB4IDA7XHJcbn1cclxuXHJcbi5tMGUxNSB7XHJcbiAgICBtYXJnaW46IDAgMTVweDtcclxufVxyXG5cclxuLm0yMGUwIHtcclxuICAgIG1hcmdpbjogMjBweCAwO1xyXG59XHJcblxyXG4uZGl2U3BhblByb2R1dG9zRGVzdGFxdWUge1xyXG4gICAgY29sb3I6ICM2MTYxNjA7XHJcbiAgICBmb250LXdlaWdodDogNzAwO1xyXG4gICAgbGV0dGVyLXNwYWNpbmc6IDVweDtcclxuICAgIGZvbnQtc2l6ZTogMS4yNXJlbTtcclxuICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjNjE2MTYwO1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMTVweDtcclxuICAgIHBhZGRpbmctYm90dG9tOiAxMHB4O1xyXG59XHJcblxyXG4uZGl2Q29sc1Byb2R1dG9zRGVzdGFxdWUge1xyXG4gICAgZGlzcGxheTogZ3JpZDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIHBhZGRpbmc6IDE1cHg7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgICBtaW4td2lkdGg6IDI0MHB4O1xyXG59XHJcblxyXG4uYnRuQ29tcHJhciB7XHJcbiAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMUJCMjU4O1xyXG4gICAgY29sb3I6ICNmZmY7XHJcbiAgICBib3JkZXItcmFkaXVzOiA2cHg7XHJcbiAgICBib3JkZXI6IG5vbmU7XHJcbiAgICBwYWRkaW5nOiAxMHB4IDA7XHJcbiAgICBtYXJnaW4tdG9wOiAzMHB4O1xyXG4gICAgbWF4LWhlaWdodDogNDFweDtcclxufVxyXG5cclxuICAgIC5idG5Db21wcmFyOmhvdmVyIHtcclxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMTM5NDQ3O1xyXG4gICAgfVxyXG5cclxuLmRpdlByb2R1dG9FbURlc3RhcXVlIHtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xyXG4gICAgcGFkZGluZzogMTBweCAyNXB4O1xyXG4gICAgYm9yZGVyLXJhZGl1czogM3B4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxufVxyXG5cclxuLmNvckZ1bmRvTGluaGFQcm9kdXRvc0Rlc3RhcXVlIHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNlY2VjZWM7XHJcbn1cclxuXHJcbi5ib3JkYUNpbnphIHtcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkICNhN2E3YTQ7XHJcbn0iXX0= */");

/***/ }),

/***/ "./src/app/home/home.component.ts":
/*!****************************************!*\
  !*** ./src/app/home/home.component.ts ***!
  \****************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let HomeComponent = class HomeComponent {
    constructor() { }
    ngOnInit() {
    }
};
HomeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-home',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./home.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/home/home.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./home.component.css */ "./src/app/home/home.component.css")).default]
    })
], HomeComponent);



/***/ }),

/***/ "./src/app/home/home.module.ts":
/*!*************************************!*\
  !*** ./src/app/home/home.module.ts ***!
  \*************************************/
/*! exports provided: HomeModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeModule", function() { return HomeModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _home_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./home.component */ "./src/app/home/home.component.ts");
/* harmony import */ var _home_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./home.routing.module */ "./src/app/home/home.routing.module.ts");





let HomeModule = class HomeModule {
};
HomeModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [
            _home_component__WEBPACK_IMPORTED_MODULE_3__["HomeComponent"]
        ],
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _home_routing_module__WEBPACK_IMPORTED_MODULE_4__["HomeRoutingModule"]
        ]
    })
], HomeModule);



/***/ }),

/***/ "./src/app/home/home.routing.module.ts":
/*!*********************************************!*\
  !*** ./src/app/home/home.routing.module.ts ***!
  \*********************************************/
/*! exports provided: HomeRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeRoutingModule", function() { return HomeRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _home_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./home.component */ "./src/app/home/home.component.ts");




const routes = [
    {
        path: '',
        component: _home_component__WEBPACK_IMPORTED_MODULE_3__["HomeComponent"]
    }
];
let HomeRoutingModule = class HomeRoutingModule {
};
HomeRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [],
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], HomeRoutingModule);



/***/ })

}]);
//# sourceMappingURL=home-home-module-es2015.js.map