(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["loja-loja-module"], {
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/loja/loja.component.html": 
        /*!********************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/loja/loja.component.html ***!
          \********************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<div>\r\n    <p>EM BREVE NOSSA LOJA</p>\r\n</div>");
            /***/ 
        }),
        /***/ "./src/app/loja/loja.component.css": 
        /*!*****************************************!*\
          !*** ./src/app/loja/loja.component.css ***!
          \*****************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("div {\r\n    text-align: center;\r\n    margin: 4% 0 5%;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbG9qYS9sb2phLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxrQkFBa0I7SUFDbEIsZUFBZTtBQUNuQiIsImZpbGUiOiJzcmMvYXBwL2xvamEvbG9qYS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiZGl2IHtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIG1hcmdpbjogNCUgMCA1JTtcclxufSJdfQ== */");
            /***/ 
        }),
        /***/ "./src/app/loja/loja.component.ts": 
        /*!****************************************!*\
          !*** ./src/app/loja/loja.component.ts ***!
          \****************************************/
        /*! exports provided: LojaComponent */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LojaComponent", function () { return LojaComponent; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            var LojaComponent = /** @class */ (function () {
                function LojaComponent() {
                }
                LojaComponent.prototype.ngOnInit = function () {
                };
                return LojaComponent;
            }());
            LojaComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'app-loja',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./loja.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/loja/loja.component.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./loja.component.css */ "./src/app/loja/loja.component.css")).default]
                })
            ], LojaComponent);
            /***/ 
        }),
        /***/ "./src/app/loja/loja.module.ts": 
        /*!*************************************!*\
          !*** ./src/app/loja/loja.module.ts ***!
          \*************************************/
        /*! exports provided: LojaModule */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LojaModule", function () { return LojaModule; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
            /* harmony import */ var _loja_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./loja.component */ "./src/app/loja/loja.component.ts");
            /* harmony import */ var _loja_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./loja.routing.module */ "./src/app/loja/loja.routing.module.ts");
            var LojaModule = /** @class */ (function () {
                function LojaModule() {
                }
                return LojaModule;
            }());
            LojaModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
                    declarations: [_loja_component__WEBPACK_IMPORTED_MODULE_3__["LojaComponent"]],
                    imports: [
                        _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                        _loja_routing_module__WEBPACK_IMPORTED_MODULE_4__["LojaRoutingModule"]
                    ]
                })
            ], LojaModule);
            /***/ 
        }),
        /***/ "./src/app/loja/loja.routing.module.ts": 
        /*!*********************************************!*\
          !*** ./src/app/loja/loja.routing.module.ts ***!
          \*********************************************/
        /*! exports provided: LojaRoutingModule */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LojaRoutingModule", function () { return LojaRoutingModule; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
            /* harmony import */ var _loja_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./loja.component */ "./src/app/loja/loja.component.ts");
            var routes = [
                {
                    path: '',
                    component: _loja_component__WEBPACK_IMPORTED_MODULE_3__["LojaComponent"]
                }
            ];
            var LojaRoutingModule = /** @class */ (function () {
                function LojaRoutingModule() {
                }
                return LojaRoutingModule;
            }());
            LojaRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
                    declarations: [],
                    imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
                    exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
                })
            ], LojaRoutingModule);
            /***/ 
        })
    }]);
//# sourceMappingURL=loja-loja-module-es2015.js.map
//# sourceMappingURL=loja-loja-module-es5.js.map
//# sourceMappingURL=loja-loja-module-es5.js.map