(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["contato-contato-module"], {
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/contato/contato.component.html": 
        /*!**************************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/contato/contato.component.html ***!
          \**************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<div>\r\n    <p>EM BREVE NOSSA ÁREA DE CONTATO</p>\r\n</div>");
            /***/ 
        }),
        /***/ "./src/app/contato/contato.component.css": 
        /*!***********************************************!*\
          !*** ./src/app/contato/contato.component.css ***!
          \***********************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("div {\r\n    text-align: center;\r\n    margin: 4% 0 5%;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29udGF0by9jb250YXRvLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxrQkFBa0I7SUFDbEIsZUFBZTtBQUNuQiIsImZpbGUiOiJzcmMvYXBwL2NvbnRhdG8vY29udGF0by5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiZGl2IHtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIG1hcmdpbjogNCUgMCA1JTtcclxufSJdfQ== */");
            /***/ 
        }),
        /***/ "./src/app/contato/contato.component.ts": 
        /*!**********************************************!*\
          !*** ./src/app/contato/contato.component.ts ***!
          \**********************************************/
        /*! exports provided: ContatoComponent */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContatoComponent", function () { return ContatoComponent; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            var ContatoComponent = /** @class */ (function () {
                function ContatoComponent() {
                }
                ContatoComponent.prototype.ngOnInit = function () {
                };
                return ContatoComponent;
            }());
            ContatoComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'app-contato',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./contato.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/contato/contato.component.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./contato.component.css */ "./src/app/contato/contato.component.css")).default]
                })
            ], ContatoComponent);
            /***/ 
        }),
        /***/ "./src/app/contato/contato.module.ts": 
        /*!*******************************************!*\
          !*** ./src/app/contato/contato.module.ts ***!
          \*******************************************/
        /*! exports provided: ContatoModule */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContatoModule", function () { return ContatoModule; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
            /* harmony import */ var _contato_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./contato.component */ "./src/app/contato/contato.component.ts");
            /* harmony import */ var _loja_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./loja.routing.module */ "./src/app/contato/loja.routing.module.ts");
            var ContatoModule = /** @class */ (function () {
                function ContatoModule() {
                }
                return ContatoModule;
            }());
            ContatoModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
                    declarations: [_contato_component__WEBPACK_IMPORTED_MODULE_3__["ContatoComponent"]],
                    imports: [
                        _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                        _loja_routing_module__WEBPACK_IMPORTED_MODULE_4__["ContatoRoutingModule"]
                    ]
                })
            ], ContatoModule);
            /***/ 
        }),
        /***/ "./src/app/contato/loja.routing.module.ts": 
        /*!************************************************!*\
          !*** ./src/app/contato/loja.routing.module.ts ***!
          \************************************************/
        /*! exports provided: ContatoRoutingModule */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContatoRoutingModule", function () { return ContatoRoutingModule; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
            /* harmony import */ var _contato_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./contato.component */ "./src/app/contato/contato.component.ts");
            var routes = [
                {
                    path: '',
                    component: _contato_component__WEBPACK_IMPORTED_MODULE_3__["ContatoComponent"]
                }
            ];
            var ContatoRoutingModule = /** @class */ (function () {
                function ContatoRoutingModule() {
                }
                return ContatoRoutingModule;
            }());
            ContatoRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
                    declarations: [],
                    imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
                    exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
                })
            ], ContatoRoutingModule);
            /***/ 
        })
    }]);
//# sourceMappingURL=contato-contato-module-es2015.js.map
//# sourceMappingURL=contato-contato-module-es5.js.map
//# sourceMappingURL=contato-contato-module-es5.js.map