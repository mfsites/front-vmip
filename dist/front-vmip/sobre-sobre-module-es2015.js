(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["sobre-sobre-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/sobre/sobre.component.html":
/*!**********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/sobre/sobre.component.html ***!
  \**********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"content\">\r\n    <h5>Quem Somos:</h5>\r\n    <p>\r\n        A VMIP-Calibração, Venda e Manutenção de Instrumentos de Precisão é uma empresa que atua no ramo de metrologia realizando serviços de calibração, \r\n        manutenção e venda de instrumento de medição.<br>\r\n        Histórico: Está no mercado desde 2005, com foco principal em vendas de instrumento e materiais diversos.<br>\r\n        A partir do mês de Março de 2012, para satisfazer as necessidade do mercado e dos nossos investidores, agregamos um novo negócio:<br>\r\n        MANUTENÇÃO E CALIBRAÇÃO DE INSTRUMENTOS DE PRECISÃO.<br>\r\n        Nossa empresa está engajada em um sistema ético e sustentável com conceito dos 3 R`s: RECICLAR,REAPROVEITAR E REFORMAR, \r\n        ajudando a melhorar nosso ambiente de vida e trabalho.\r\n    </p>\r\n    <h5>Missão, Visão, Objetivo e Valores</h5>\r\n    <p>\r\n        Missão:<br>\r\n        A empresa VMIP é orientada pela responsabilidade e confiabilidade dos nossos serviços, proporcionando a satisfação a todos stakeholders.<br>\r\n        Visão: Ser referencia na prestação de serviços em calibração, manutenção preventiva e corretiva e também em vendas de instrumentos de precisão para todo o Brasil.<br><br>\r\n        Objetivos:<br>\r\n        Agregar valores e buscar a total satisfação dos nossos stakeholders<br>\r\n        Obter a excelência na qualidade em prestação de serviços <br><br>\r\n        Valores:<br>\r\n        Respeito ao Meio ambiente.<br>\r\n        Responsabilidade Socio-Ambiental.<br>\r\n        Ética.\r\n    </p>\r\n</div>");

/***/ }),

/***/ "./src/app/sobre/servico.routing.module.ts":
/*!*************************************************!*\
  !*** ./src/app/sobre/servico.routing.module.ts ***!
  \*************************************************/
/*! exports provided: SobreRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SobreRoutingModule", function() { return SobreRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _sobre_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./sobre.component */ "./src/app/sobre/sobre.component.ts");




const routes = [
    {
        path: '',
        component: _sobre_component__WEBPACK_IMPORTED_MODULE_3__["SobreComponent"]
    }
];
let SobreRoutingModule = class SobreRoutingModule {
};
SobreRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [],
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], SobreRoutingModule);



/***/ }),

/***/ "./src/app/sobre/sobre.component.css":
/*!*******************************************!*\
  !*** ./src/app/sobre/sobre.component.css ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".content {\r\n    color: #05053c;\r\n    padding: 0 50px 30px;\r\n}\r\n\r\n.content h5 {\r\n    font-weight: bold;\r\n    margin-bottom: 1.5rem;\r\n}\r\n\r\n.content p {\r\n    padding-left: 10px;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc29icmUvc29icmUuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLGNBQWM7SUFDZCxvQkFBb0I7QUFDeEI7O0FBRUE7SUFDSSxpQkFBaUI7SUFDakIscUJBQXFCO0FBQ3pCOztBQUVBO0lBQ0ksa0JBQWtCO0FBQ3RCIiwiZmlsZSI6InNyYy9hcHAvc29icmUvc29icmUuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jb250ZW50IHtcclxuICAgIGNvbG9yOiAjMDUwNTNjO1xyXG4gICAgcGFkZGluZzogMCA1MHB4IDMwcHg7XHJcbn1cclxuXHJcbi5jb250ZW50IGg1IHtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMS41cmVtO1xyXG59XHJcblxyXG4uY29udGVudCBwIHtcclxuICAgIHBhZGRpbmctbGVmdDogMTBweDtcclxufSJdfQ== */");

/***/ }),

/***/ "./src/app/sobre/sobre.component.ts":
/*!******************************************!*\
  !*** ./src/app/sobre/sobre.component.ts ***!
  \******************************************/
/*! exports provided: SobreComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SobreComponent", function() { return SobreComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let SobreComponent = class SobreComponent {
    constructor() { }
    ngOnInit() {
    }
};
SobreComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-sobre',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./sobre.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/sobre/sobre.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./sobre.component.css */ "./src/app/sobre/sobre.component.css")).default]
    })
], SobreComponent);



/***/ }),

/***/ "./src/app/sobre/sobre.module.ts":
/*!***************************************!*\
  !*** ./src/app/sobre/sobre.module.ts ***!
  \***************************************/
/*! exports provided: SobreModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SobreModule", function() { return SobreModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _sobre_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./sobre.component */ "./src/app/sobre/sobre.component.ts");
/* harmony import */ var _servico_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./servico.routing.module */ "./src/app/sobre/servico.routing.module.ts");





let SobreModule = class SobreModule {
};
SobreModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [_sobre_component__WEBPACK_IMPORTED_MODULE_3__["SobreComponent"]],
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _servico_routing_module__WEBPACK_IMPORTED_MODULE_4__["SobreRoutingModule"]
        ]
    })
], SobreModule);



/***/ })

}]);
//# sourceMappingURL=sobre-sobre-module-es2015.js.map